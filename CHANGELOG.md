## 3.7.0 (2020-09-25)

- Added possibility to provide multiple source files.
- Expanded CLI to instruct font file directly.
- Added ifdef mechanism, see --define option.

## 3.6.0 (2020-07-08)

- Removed automatic storage index assignment from WS instruction.
- Added storage block, to manually declare storage index identifiers.
- If a cvt, storage, or function identifier can be converted to an
  integer, it is used as an index directly.

## 3.5.0 (2020-05-08)

- Added support for raw DELTA instructions.
- Fixed composite glyphs for FontTools.
- To accommodate unusual glyph names by FontTools, comments must be
  preceded by whitespace if they do not appear at the beginning of a line.
