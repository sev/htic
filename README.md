Humble Type Instruction Compiler
================================

The Humble Type Instruction Compiler translates simplified
plain-text instructions into optimized [TrueType] bytecode,
while taking care of cumbersome tasks like consolidating
stack pushes or mapping indices.

Importantly, humble type instructions are not intended to be a
new language, but rather provide a more convenient arrangement
of instruction names, flags, and arguments.

The compiler is implemented in Python 3. It supports the
instruction of [FontForge] and [FontTools] font objects.
With thanks to Jens Kutilek for adding FontTools support.

Further documentation is available in the [doc](doc) directory:

- [format.md](doc/format.md): Introduction to the instruction format
- [blocks.md](doc/blocks.md): How the instructions are organized in blocks
- [instructions.md](doc/instructions.md): Overview of supported instructions
- [example.md](doc/example.md): An example glyph program for the letter H
- [usage.md](doc/usage.md): How to compile the instructions with Python
- [grammar.abnf](doc/grammar.abnf): The grammar for the instruction format


[TrueType]:  https://docs.microsoft.com/typography/opentype/spec/
[FontForge]: https://fontforge.github.io
[FontTools]: https://github.com/fonttools/fonttools
