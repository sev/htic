Blocks
======

Humble code is organized in blocks. A block starts with a name
and encloses its content in curly brackets. Depending on the block,
the content consists of humble type instructions, or represents
a related TrueType table.

Blocks must be unique. Blocks must be defined before their
content is referenced. For example, the control values can
only be referenced after the `cvt` block is defined.


flags
-----

Writing instruction flags in binary form can be cumbersome.
Therefore, this block allows to define flag names. Each line starts
with a binary flag value, followed by the corresponding name.

```
flags
{
  0 y
  1 x
  1 rnd
  01101 stem
  01101 pMRB
}
```


head
----

This block represents the [head] table. Each line starts with a value,
followed by its identifier. The example lists all supported identifiers.
The `flags` bitmask is split into individual bit values.
Note that the compiler cannot currently validate the values, the font
author is responsible for their correctness.

```
head
{
  0 flags.instructionsMayDependOnPointSize  # Bit 2
  1 flags.forcePpemToIntegerValues          # Bit 3
  1 flags.instructionsMayAlterAdvanceWidth  # Bit 4
  0 flags.fontOptimizedForClearType         # Bit 13
  9 lowestRecPPEM
}
```


maxp
----

This block represents the [maxp] table. Each line starts with a value,
followed by its identifier. The example lists all supported identifiers.
Note that the compiler cannot currently validate the values, the font
author is responsible for their correctness.

```
maxp
{
  256 maxStackElements
   32 maxFunctionDefs
   32 maxStorage
    2 maxZones
   16 maxTwilightPoints
}
```


gasp
----

This block represents the [gasp] table, version 1. Each line starts
with a size, followed by optional flags that apply up to that size.
Available flags are `doGridfit`, `doGray`, `symSmoothing`, `symGridfit`.

```
gasp
{
      7           doGray
  65535 doGridfit doGray symSmoothing symGridfit
}
```


cvt
---

This block represents the [cvt] table. Each line starts with
a control value, followed by its identifier.

```
cvt
{
  -80 descender
  160 stem
  700 cap
}
```


storage
-------

This block declares identifiers for storage indices.
Each line starts with an index, followed by its identifier.
A storage index can have multiple identifiers.

```
storage
{
  3 foo
  7 bar
  7 qux
}
```


Program blocks
--------------

Humble type instructions for the [fpgm] and [prep] tables are placed
in blocks with the corresponding name. Glyph instructions are placed
in a block with the name of the glyph, as defined by the font.

```
fpgm
{
  FDEF setRoundState
    RTDG
  ENDF
}

prep
{
  IF ((8 > (MPPEM)) or (GETINFO 0b110))
    INSTCTRL 1 1
  EIF
}

asciitilde
{
  CALL setRoundState
  MDAP[r] 0
}
```


[head]: https://docs.microsoft.com/typography/opentype/spec/head
[maxp]: https://docs.microsoft.com/typography/opentype/spec/maxp
[cvt]:  https://docs.microsoft.com/typography/opentype/spec/cvt
[gasp]: https://docs.microsoft.com/typography/opentype/spec/gasp
[fpgm]: https://docs.microsoft.com/typography/opentype/spec/fpgm
[prep]: https://docs.microsoft.com/typography/opentype/spec/prep
