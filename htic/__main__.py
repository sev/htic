"""The Humble Type Instruction Compiler translates simplified
plain-text instructions into optimized TrueType bytecode."""

import argparse
import htic


def main():

	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument("--version", action="version", version=htic.__version__)
	parser.add_argument("-f", "--font", metavar="FONT", type=str,
		help="target font file")
	parser.add_argument("-D", "--define", metavar='NAME', type=str, action="append",
		help="remove any occurence of '#ifdef NAME ' to uncomment the rest of the line")
	parser.add_argument("HTI", nargs="+",
		help="instruction file")

	args = parser.parse_args()

	if args.font:
		from fontTools.ttLib import TTFont
		font = TTFont(args.font)
		htic.toFontTools(args.HTI, font, args.define)
		font.save(args.font)
	else:
		htic.toConsole(args.HTI, args.define)


if __name__ == '__main__':
	main()
