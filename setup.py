import pathlib
import setuptools


def readVersion(relPath):
	path = pathlib.Path(__file__).parent.joinpath(relPath)
	with open(path) as stream:
		for line in stream:
			if line.startswith("__version__"):
				return line.replace('"', "'").split("'")[1]
	raise RuntimeError("Could not find version string")


setuptools.setup(
	name="htic",
	version=readVersion("htic/__init__.py"),
	description="The Humble Type Instruction Compiler translates simplified plain-text instructions into optimized TrueType bytecode.",
	url="https://gitlab.com/sev/htic",
	author="Severin Meyer",
	author_email="hello@sev.dev",
	license="MIT",
	packages=["htic"],
	entry_points={"console_scripts": ["htic = htic.__main__:main"]},
	python_requires=">=3",
	test_suite="test",
)
